import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public token: any;
  public id: any;
  public user: any = undefined;
  public user_lc: any = {};
  public config_global: any = {};

  constructor(

    private _clienteService: ClienteService,
    private _router: Router)
  
  {//inicializacion de variables
    this.token = localStorage.getItem('token');
    this.id = localStorage.getItem('id');

    this._clienteService.obtener_config_publico().subscribe(
      response=>{
        this.config_global = response.data;
        console.log(this.config_global);
      }
    )
    
    if (localStorage.getItem('user_data')) {
      // this.user_lc = JSON.parse(localStorage.getItem('user_data'));
      }else{
        this.user_lc = undefined;
      }

      this._clienteService.obtener_cliente_guest(this.id ,this.token).subscribe(    
        response=>{
  
          this.user = response.data;
          //console.log( this.user);
          localStorage.setItem('user_data',JSON.stringify(this.user));
        },
        error=>{
          console.log(error);
          this.user = undefined;
        }
      )
     }

  ngOnInit(): void {
  }

  logout() {
    
    window.location.reload(); //para refrescar la pagina
    localStorage.clear();
    this._router.navigate(['/']);
  }    


}
